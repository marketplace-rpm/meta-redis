%global app                     redis
%global user                    %{app}
%global group                   %{app}

%global d_home                  /home
%global d_storage               %{d_home}/storage_01
%global d_data                  %{d_storage}/data_02

Name:                           meta-redis
Version:                        1.0.0
Release:                        9%{?dist}
Summary:                        META-package for install and configure Redis
License:                        GPLv3

Source10:                       %{app}.local.conf

Requires:                       redis

%description
META-package for install and configure Redis.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -dp -m 0755 %{buildroot}%{d_data}/%{app}

%{__install} -Dp -m 0644 %{SOURCE10} \
  %{buildroot}%{_sysconfdir}/%{app}.local.conf


%files
%attr(0700,%{user},%{group}) %dir %{d_data}/%{app}
%config %{_sysconfdir}/%{app}.local.conf


%changelog
* Sun Jul 28 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-9
- UPD: SPEC-file.

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-8
- UPD: SPEC-file.

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-7
- UPD: SPEC-file.

* Fri Apr 19 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-6
- UPD: Directory structure.

* Sun Apr 14 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-5
- UPD: Directory structure.

* Sat Apr 13 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-4
- NEW: 1.0.0-4.

* Wed Apr 10 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-3
- NEW: 1.0.0-3.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-2
- NEW: 1.0.0-2.

* Wed Feb 13 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
